#ReorganizeCSV
=============

Re-organize cells in CSV file from x cells in a group to one row of x columns, each successive group become a new row.

##Arguments

```
ReorganizeCSV.py --help
ReorganizeCSV.py -h
ReorganizeCSV.py --input-file=<filename> --output-file=<filename> --rows-to-colums=<integer>
ReorganizeCSV.py -i <filename> -o <filename> -r <integer>
```

##Examples

See the CSV files in the "Test data" folder for example input and output files.

The output files are produced from the input files like so:
```
ReorganizeCSV.py --input-file=input2col.csv --output-file=output2col.csv --rows-to-colums=2
ReorganizeCSV.py --input-file=input3col.csv --output-file=output3col.csv --rows-to-colums=3
```

or, more concisely, like so:
```
ReorganizeCSV.py -i input2col.csv -o output2col.csv -r 2
ReorganizeCSV.py -i input3col.csv -o output3col.csv -r 3
```
