﻿#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Python before 3.3.3 has some unicode issues.
# Partial workaround in examples at end of:
# http://docs.python.org/3.0/library/csv.html

# http://docs.python.org/3.3/library/csv.html#csv-examples
# mention no such issue for Python 3.3.3

import csv
import sys
import getopt


def read_input_file(fname):
    # Read input file
    input_data = []
    with open(fname, "r", newline='', encoding="utf-8") as inputcsv:
        for row in csv.reader(inputcsv, dialect="excel"):
            input_data.append(row)
    return input_data


def write_output_file(fname, data):
    # Write output file
    with open(fname, "w", newline='', encoding="utf-8") as outputcsv:
        csvwriter = csv.writer(outputcsv, dialect="excel")
        csvwriter.writerows(data)


def process_data(data, rows_per_group):
    # Move cells around

    processed_data = []

    column = 1
    new_row = []
    for row in data:
        print(data)
        if column <= rows_per_group:
            # Add cell to output row
            new_row.append(row[0])
            column = column + 1
            
            # Also save last row to processed_data
            if (len(data) / rows_per_group) == (len(processed_data) + 1):
                processed_data.append(new_row)
        else:
            # Save row to processed_data
            processed_data.append(new_row)
            
            # Start new row
            new_row = []
            column = 1
            
            # Add cell to output row
            new_row.append(row[0])
            column = column + 1
            
    return processed_data


def main(argv):
    help_text = """
Re-organize cells in CSV file from x cells in a group to one row of x columns, each successive group become a new row.

Usage: """ + sys.argv[0] + """ --help
Usage: """ + sys.argv[0] + """ -h

Usage: """ + sys.argv[0] + """ --input-file=<filename> --output-file=<filename> --rows-to-colums=<integer>
Usage: """ + sys.argv[0] + """ -i <filename> -o <filename> -r <integer>
"""
    input_filename = ""
    output_filename = ""
    rows_per_group = -1

    # Define command-line arguments
    try:
        opts, args = getopt.getopt(argv,
                                   "hi:o:r:",
                                   ["help",
                                    "input-file=",
                                    "output-file=",
                                    "rows-to-colums="])
    except getopt.GetoptError:
        print(help_text)
        sys.exit(2)

    # Parse command-line arguments
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print(help_text)
            sys.exit()
        if opt in ("-i", "--input-file"):
            input_filename = arg
        if opt in ("-o", "--output-file"):
            output_filename = arg
        if opt in ("-r", "--rows-to-colums"):
            rows_per_group = int(arg)

    # Check that all required arguments were given
    if input_filename and output_filename and rows_per_group > 0:
        # Do the work
        input_data = read_input_file(input_filename)
        processed_data = process_data(input_data, rows_per_group)
        write_output_file(output_filename, processed_data)
    else:
        print(help_text)
        sys.exit(2)

if __name__ == "__main__":
   main(sys.argv[1:])
